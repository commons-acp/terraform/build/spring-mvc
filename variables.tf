variable "gitlab_package_url" {
  description = ""
}

variable "gitlab_api_token" {
  description = ""
}

variable "source_folder" {
  default = "war_folder/"
  description = ""
}

variable "env_vars" {
  type = map(string)
  description = ""
}


variable "app_war_name_output" {
  description = ""
}


variable "source_template" {
  description = ""
}
