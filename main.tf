

data "atn-utils_gitlab_package" "war" {
  repository_url = var.gitlab_package_url
  access_token   = var.gitlab_api_token
  output_path    = var.source_folder
  with_extract   = true
}



data "template_file" "template" {
  depends_on = [data.atn-utils_gitlab_package.war]
  template   = file(var.source_template)
  vars       = var.env_vars
}


resource "local_file" "config_properties" {
  depends_on  =[data.template_file.template]
  content     = data.template_file.template.rendered
  filename    = "${var.source_folder}/WEB-INF/classes/config.properties"
}




data "archive_file" "war" {
  depends_on =[local_file.config_properties]
  type        = "zip"
  source_dir  = var.source_folder
  output_path = "${var.app_war_name_output}.war"
}


