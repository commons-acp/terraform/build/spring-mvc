
terraform {
  required_version = ">= 0.13"

  required_providers {
    atn-utils = {
      source = "allence-tunisie/atn-utils"
    }
    archive = {
      source = "hashicorp/archive"
      version = "2.1.0"
    }
    template = {
      source = "hashicorp/template"
      version = "2.2.0"
    }
  }
}

provider "atn-utils" {

}
provider "archive" {

}
provider "template" {

}
